# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtWidgets import QLineEdit

class LineEdit(QLineEdit):
    showTextCount = QtCore.pyqtSignal(str)   #定义信号

    def __init__(self, parent):
        super().__init__(parent)
        


    def focusInEvent(self, event):        
        super().focusInEvent(event)
        self.showTextCount.emit(self.text())

    def mousePressEvent(self,event):        
        super().mousePressEvent(event)
        self.showTextCount.emit(self.text())

