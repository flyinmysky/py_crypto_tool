# -*- coding: utf-8 -*-

from PyQt5.Qt import QThread
from PyQt5.QtCore import pyqtSignal

class ThreadHash(QThread):
    hashOk = pyqtSignal(bytes)
    hashError = pyqtSignal(Exception)

    def __init__(self, how, data):
        super().__init__()
        self.how = how
        self.data = data

    def run(self):
        try:
            h = self.how(self.data)
            v = h.digest()
            self.hashOk.emit(v)         
        except Exception as e:
            self.hashError.emit(e)