# -*- coding: utf-8 -*-

from crypto import RSA
from PyQt5.Qt import QThread
from PyQt5.QtCore import pyqtSignal

class ThreadGenRsaKey(QThread):
    genaratedKey = pyqtSignal(tuple)
    genarateError = pyqtSignal(Exception)

    def __init__(self, modulusSize, e=65537):
        super().__init__()
        self.modulusSize = modulusSize
        self.e = e

    def run(self):
        try:
            k = RSA.genKey(self.modulusSize, e = self.e)
            self.genaratedKey.emit(k)
        except Exception as e:
            self.genarateError.emit(e)