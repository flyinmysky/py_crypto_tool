# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtWidgets import QTextEdit

class TextEdit(QTextEdit):
    showTextCount = QtCore.pyqtSignal(str)   #定义信号

    def __init__(self, parent):
        super().__init__(parent)

    def focusInEvent(self, event): 
        self.showTextCount.emit(self.toPlainText())
        super().focusInEvent(event)        

    def mousePressEvent(self,event):
        self.showTextCount.emit(self.toPlainText())      
        super().mousePressEvent(event)        

