# py_crypto_tool

#### 介绍
加解密工具箱，支持常用对称式、非对称式加解密、HASH、SM2/SM3/SM4等算法
还支持一些字符转换的小功能

#### 软件架构
主要的包：
- PYQT
- Cryptodomex
- [gmssl](https://gitee.com/mirrors/gmssl-python?_from=gitee_search)
gmssl由于原始功能并不完全满足需求，因此将源码下载下来并做了一点点修改。

#### 安装教程

```
pipenv shell
pipenv install
```


#### 使用说明

`python main.py`
`一键打包.cmd`，在pipenv下用pyinstaller打包为exe

图形界面，操作简单

#### 截图
![alt 1](doc/1.png)
![alt 2](doc/2.png)
![alt 3](doc/3.png)
![alt 4](doc/4.png)
![alt 5](doc/5.png)
![alt 6](doc/6.png)
![alt 7](doc/7.png)
![alt 8](doc/8.png)