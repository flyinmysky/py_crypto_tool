@echo off

cd src
rmdir /s /q ..\dist
rmdir /s /q ..\build
rmdir /s /q dist
rmdir /s /q build

pyinstaller -w -y -D -p ".\ui" -i .\ui\lock64.ico -n crypto_tool main.py
if not "%errorlevel%"=="0" (
    echo pyinstaller failed
    exit /B 1
)
mkdir dist\crypto_tool\res
mkdir dist\crypto_tool\doc
mkdir dist\crypto_tool\test_cases

xcopy /y /s /e .\doc dist\crypto_tool\doc\ 

xcopy /y /s /e .\res dist\crypto_tool\res\ 


xcopy /y /s /e dist ..\dist\
rmdir /s /q dist
rmdir /s /q build

echo OK
pause
